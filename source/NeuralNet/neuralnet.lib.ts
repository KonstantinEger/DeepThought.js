import Matrix from '../Matrix/matrix';
import NeuralNet from './neuralnet';

export class HiddenLayer {
    /*
     *		Template für einen HiddenLayer
     */
    public W_F_T: Matrix;
    public biases: Matrix;
    public values: Matrix;

    constructor(nodesInFront: number, numOfNodes: number) {
        this.W_F_T = new Matrix(numOfNodes, nodesInFront);
        this.W_F_T.randomize();
        this.biases = new Matrix(numOfNodes, 1);
        this.biases.randomize();
        this.values = new Matrix(numOfNodes, 1);
    }
}

export class OutputLayer {
    /*
     *		Template für einen OutputLayer
     */
    public W_F_T: Matrix;
    public biases: Matrix;

    constructor(nodesInFront: number, numOfNodes: number) {
        this.W_F_T = new Matrix(numOfNodes, nodesInFront);
        this.W_F_T.randomize();
        this.biases = new Matrix(numOfNodes, 1);
        this.biases.randomize();
    }
}

export function addHiddenLayer(layer: IlayerInput, net: NeuralNet): void {
    net.hiddenLayerNodes[net.numHiddenLayers] = layer.nodes;
    // Check if its the first hidden layer
    if (net.numHiddenLayers === 0) {
        net.hiddenLayers[net.numHiddenLayers] =
        new HiddenLayer(net.inputNodes, net.hiddenLayerNodes[net.numHiddenLayers]);
    } else {
        net.hiddenLayers[net.numHiddenLayers] =
        new HiddenLayer(net.hiddenLayerNodes[net.numHiddenLayers - 1], net.hiddenLayerNodes[net.numHiddenLayers]);
    }
    net.numHiddenLayers++;
}

export function addOutputLayer(layer: IlayerInput, net: NeuralNet): void {
    net.outputNodes = layer.nodes;
    net.outputLayer = new OutputLayer(net.hiddenLayerNodes[net.numHiddenLayers - 1], net.outputNodes);
}

export function load(data: string): NeuralNet {
    const obj = JSON.parse(data);
    const net = new NeuralNet();

    net.inputNodes = obj.inputNodes;
    for (let i = 0; i < obj.hiddenLayerNodes.length; i++) {
        net.hiddenLayerNodes[i] = obj.hiddenLayerNodes[i];
    }
    for (let i = 0; i < obj.hiddenLayers.length; i++) {
        net.hiddenLayers[i] = obj.hiddenLayers[i];
    }
    net.numHiddenLayers = obj.numHiddenLayers;
    net.outputNodes = obj.outputNodes;
    net.outputLayer = obj.outputLayer;
    net.learningRate = obj.learningRate;

    return net;
}

export interface ITestOps {
    iterations?: number;
    print?: boolean;
    passWhen?: number;
}
export interface ITrainOps {
    iterations?: number;
    print?: boolean;
    test?: boolean;
}

export function pickRandomFromArray(array: ITrainingInput[]): ITrainingInput {
    return array[Math.floor(Math.random() * array.length)];
}

export interface ITrainingInput {
    input: number[];
    target: number[];
}

export interface IlayerInput {
    type: 'input'|'hidden'|'output';
    nodes: number;
    randomized?: boolean;
}

export function sigmoid(x: number): number {
    /*
     *		Activation-Function
     */
    return 1 / (1 + Math.exp(-x));
}

export function dsigmoid(y: number): number {
    return y * (1 - y);
}
