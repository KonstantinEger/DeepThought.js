import chalk from 'chalk';

import Matrix from '../Matrix/matrix';
import * as comps from './neuralnet.lib';

/**
 * @class Neurales Netzwerk für Machine-Learning.
 * @constructs NeuralNet: Baut ein leeres Netzwerk auf.
 * @method addLayers: Layer zum Netzwerk hinzufügen.
 * @method predict: Feed-Forward Algorythmus.
 * @method train: Trainiert das Netzwerk.
 * @method test: Testet das Netzwerk.
 * @method mutate: Mutate-Funktion für gen. Algorythmen.
 * @method static log: Loggt ein gegebenes Netzwerk zur Konsole.
 * @method log: Loggt dieses Netzwerk zur Konsole.
 * @method emit: Wandelt das Netzwerk in JSON Format um.
 */
export default class NeuralNet {
    public hiddenLayers: comps.HiddenLayer[];
    public hiddenLayerNodes: number[];
    public inputNodes: number;
    public learningRate: number;
    public numHiddenLayers: number;
    public outputLayer: comps.OutputLayer;
    public outputNodes: number;

    private totalError: number;

    /**
     * Leeres Neurales Netzwerk aufbauen.
     * @returns Ein leeres Neurales Netzwerk.
     */
    public constructor() {
        this.inputNodes = 0; // anzahl der Input-nodes
        this.hiddenLayerNodes = []; // Array welches die anzahl der nodes für jd. hL speichert
        this.hiddenLayers = []; // init für das array mit den hiddenLayer objekten
        this.numHiddenLayers = 0; // anzahl der hiddenLayer
        this.outputNodes = 0; // anzahl der outputNodes
        this.outputLayer = new comps.OutputLayer(0, 0);

        this.learningRate = 0.1; // Preset für die learningRate
        this.totalError = 0;
    }

    /**
     * Funktion die es für den User einfacher macht sein Network aufzustellen.
     * Eine Art redirect zu den spezifischeren Funktionen.
     * @param layerArray Ein Array aus Objekten welche informationen über die Struktur des Netzwerkes
     * enthalten
     */
    public addLayers(layerArray: comps.IlayerInput[]): void {
        for (const layer of layerArray) {
            // let layer = layerArray[i];
            if (layer.type === 'input') {
                this.inputNodes = layer.nodes;
            } else if (layer.type === 'hidden') {
                comps.addHiddenLayer(layer, this);
            } else if (layer.type === 'output') {
                comps.addOutputLayer(layer, this);
            } else {
                console.log(chalk.redBright('Invalid layertype: ' + layer.type));
            }
        }
    }

    /**
     * Diese Funktion ist ein feed-forward Algorythmus durch das Netzwerk.
     * @param inputArray Ein Array aus Zahlen mit denen das Netzwerk sein Ergebnis berechnet.
     * @callback callback Eine Callback-Funktion welche nach dem Durchlauf aufgerufen wird und der das Ergebnis
     * übergeben wird.
     * @returns Ein Array mit den Ergebnissen des Netzwerkes.
     */
    public predict(inputArray: number[], callback?: (result: number[]) => void): number[] | void {
        if (inputArray.length !== this.inputNodes) {
            const errorText = 'Number of given inputs (' + inputArray.length + ') does not' +
                'match up with number of input nodes ' + this.inputNodes + ').';
            console.log(chalk.redBright(errorText));
            return;
        }

        const inputs = Matrix.fromArray(inputArray);
        let hidden = Matrix.multiply(this.hiddenLayers[0].W_F_T, inputs);
        hidden = Matrix.add(hidden, this.hiddenLayers[0].biases);
        hidden.map(comps.sigmoid);

        for (let i = 1; i < this.hiddenLayers.length; i++) {
            hidden = Matrix.multiply(this.hiddenLayers[i].W_F_T, hidden);
            hidden = Matrix.add(hidden, this.hiddenLayers[i].biases);
            hidden.map(comps.sigmoid);
        }

        let output = Matrix.multiply(this.outputLayer.W_F_T, hidden);
        output = Matrix.add(output, this.outputLayer.biases);
        output.map(comps.sigmoid);

        /**
         *      Gibt den Output als Array zurück, z.B:
         *      <- [1, 0]
         *      -> [0.996183321413]
         */

        if (callback) {
            callback(Matrix.toArray(output));
        } else {
            return Matrix.toArray(output);
        }
    }

    /**
     * Ein backwards-propagation Algorythmus der einen Input und ein gewünschtes Ergebnis als Array
     * nimmt und über eine optionale Anzahl (default 10000) an iterations das Training durchführt.
     * @param trainingData Array mit Objekten welche als Eigenschaften die Informationen zum Training enthalten. Das
     * Netzwerk benutzt zufällig eines aus diesem Array.
     * @param ops Ein Objekt mit Optionen für den Ablauf des trainings.
     */
    public train(trainingData: comps.ITrainingInput[], ops?: comps.ITrainOps): void {
        if (ops === undefined) {
            ops = {
                iterations: 1000000,
                print: true,
                test: true
            };
        }
        if (!ops.iterations) {
            ops.iterations = 1000000;
        }
        if (ops.print === undefined) {
            ops.print = true;
        }
        if (ops.test === undefined) {
            ops.test = true;
        }

        if (ops.print === true) {
            console.clear();
            console.log(chalk.yellowBright('Training NeuralNet ...'));
        }

        for (let x = 0; x < ops.iterations; x++) {

            const data = comps.pickRandomFromArray(trainingData);

            if (data.input.length !== this.inputNodes) {
                const errorText = 'Number of given inputs (' + data.input.length + ') does not' +
                    'match up with number of input nodes ' + this.inputNodes + ').';
                console.log(chalk.redBright(errorText));
                return;
            } else if (data.target.length !== this.outputNodes) {
                const errorText = 'Number of given targets (' + data.target.length + ') does not' +
                    'match up with number of input nodes ' + this.inputNodes + ').';
                console.log(chalk.redBright(errorText));
            }

            const inputs = Matrix.fromArray(data.input);
            let hidden = Matrix.multiply(this.hiddenLayers[0].W_F_T, inputs);
            hidden = Matrix.add(hidden, this.hiddenLayers[0].biases);
            hidden.map(comps.sigmoid);
            this.hiddenLayers[0].values = hidden;

            for (let i = 1; i < this.hiddenLayers.length; i++) {
                hidden = Matrix.multiply(this.hiddenLayers[i].W_F_T, hidden);
                hidden = Matrix.add(hidden, this.hiddenLayers[i].biases);
                hidden.map(comps.sigmoid);
                this.hiddenLayers[i].values = hidden;
            }

            let outputs = Matrix.multiply(this.outputLayer.W_F_T, hidden);
            outputs = Matrix.add(outputs, this.outputLayer.biases);
            outputs.map(comps.sigmoid);
            const targets = Matrix.fromArray(data.target);

            // TRAINING

            // Generate Output-Error
            const outputError = Matrix.subtract(targets, outputs);

            // Gradient for last weights
            const gradient = Matrix.map(outputs, comps.dsigmoid);
            gradient.multiply(outputError);
            gradient.multiply(this.learningRate);

            let hiddenT = Matrix.transpose(this.hiddenLayers[this.numHiddenLayers - 1].values);
            const weightsHODeltas = Matrix.multiply(gradient, hiddenT);

            this.outputLayer.W_F_T.add(weightsHODeltas);
            this.outputLayer.biases.add(gradient);

            const wHOT = Matrix.transpose(this.outputLayer.W_F_T);
            const hiddenErrors = Matrix.multiply(wHOT, outputError);

            for (let i = this.numHiddenLayers - 1; i >= 0; i--) {

                const hiddenGradient = Matrix.map(this.hiddenLayers[i].values, comps.dsigmoid);
                hiddenGradient.multiply(hiddenErrors);
                hiddenGradient.multiply(this.learningRate);

                // If not first hidden Layer
                if (i >= 1) {
                    hiddenT = Matrix.transpose(this.hiddenLayers[i - 1].values);
                    const weightsHHDeltas = Matrix.multiply(hiddenGradient, hiddenT);

                    this.hiddenLayers[i].W_F_T.add(weightsHHDeltas);
                    this.hiddenLayers[i].biases.add(hiddenGradient);
                } else if (i === 0) {
                    const inputsT = Matrix.transpose(inputs);
                    const weightsIHDeltas = Matrix.multiply(hiddenGradient, inputsT);

                    this.hiddenLayers[i].W_F_T.add(weightsIHDeltas);
                    this.hiddenLayers[i].biases.add(hiddenGradient);
                }
            }
            if (ops.print === true) {
                for (let i = 0; i < 20; i++) {
                    if (x === i * ops.iterations / 10 && x !== 0) {
                        const percent = i * 10;
                        console.clear();
                        console.log(chalk.yellowBright('Training NeuralNet ...'));
                        console.log(`${percent}% (${x})`);
                    }
                }
            }
        }
        if (ops.print === true) {
            console.clear();
            console.log(chalk.yellowBright('Training NeuralNet ...'));
            console.log(chalk.greenBright('Done training.'));
        }

        if (ops.test === true) {
            if (ops.print === true) {
                console.log(chalk.yellowBright('Testing ...'));
            }

            const testRes = this.test(trainingData, {
                iterations: ops.iterations / 2,
                passWhen: 60,
                print: false
            });
            this.totalError = testRes.val;
            const state = testRes.stat;

            if (ops.print === true) {
                console.log(chalk.greenBright('Done testing'));
                if (state === 'FAILED') {
                    console.log('Status: ' + chalk.redBright(state) + ' ' + this.totalError + '%');
                } else {
                    console.log('Status: ' + chalk.greenBright(state) + ' ' + this.totalError + '%');
                }
            }
        }
    }

    /**
     * Diese Methode testet das Netzwerk indem es Richtige und falsche Antworten vergleicht.
     * @param testingData Array aus Objekten wie die Trainingsdaten.
     * @param ops Optionen für den Ablauf des Testings.
     * @returns Den berechneten Prozentsatz der korrekten Antworten.
     */
    public test(testingData: comps.ITrainingInput[], ops?: comps.ITestOps): { stat: string, val: number } {
        let correct = 0;

        if (!ops) {
            ops = {
                iterations: 1000000,
                print: true
            };
        }

        if (!ops.iterations) {
            ops.iterations = 1000000;
        }
        if (ops.print === undefined) {
            ops.print = true;
        }

        if (ops.print === true) {
            console.log(chalk.yellowBright('Testing ...'));
        }

        for (let x = 0; x < ops.iterations; x++) {

            const data = comps.pickRandomFromArray(testingData);

            const inputs = Matrix.fromArray(data.input);
            let hidden = Matrix.multiply(this.hiddenLayers[0].W_F_T, inputs);
            hidden = Matrix.add(hidden, this.hiddenLayers[0].biases);
            hidden.map(comps.sigmoid);
            this.hiddenLayers[0].values = hidden;

            for (let i = 1; i < this.hiddenLayers.length; i++) {
                hidden = Matrix.multiply(this.hiddenLayers[i].W_F_T, hidden);
                hidden = Matrix.add(hidden, this.hiddenLayers[i].biases);
                hidden.map(comps.sigmoid);
                this.hiddenLayers[i].values = hidden;
            }

            let outputs = Matrix.multiply(this.outputLayer.W_F_T, hidden);
            outputs = Matrix.add(outputs, this.outputLayer.biases);
            outputs.map(comps.sigmoid);
            const targets = Matrix.fromArray(data.target);

            const shouldBeBiggest = { val: 0, index: 0 };
            const isBiggest = { val: 0, index: 0 };
            const outArr = Matrix.toArray(outputs);
            const tarArr = Matrix.toArray(targets);
            for (let f = 0; f < outArr.length; f++) {
                if (outArr[f] > isBiggest.val) {
                    isBiggest.val = outArr[f];
                    isBiggest.index = f;
                }
            }
            for (let f = 0; f < tarArr.length; f++) {
                if (tarArr[f] > shouldBeBiggest.val) {
                    shouldBeBiggest.val = tarArr[f];
                    shouldBeBiggest.index = f;
                }
            }

            if (isBiggest.index === shouldBeBiggest.index) {
                correct++;
            }
        }

        let status;
        const CORRECT_PERCENT = correct / ops.iterations * 100;
        if (ops.print !== true) {
            if (ops.passWhen === undefined) {
                status = 'PASSED';
            } else {
                if (CORRECT_PERCENT >= ops.passWhen) {
                    status = 'PASSED';
                } else {
                    status = 'FAILED';
                }
            }
        } else {
            if (ops.passWhen === undefined) {
                console.log(chalk.greenBright('Done testing'), 'Testing Results: ' + chalk.blue(`${CORRECT_PERCENT}%`) +
                    ' correct');
            } else {
                if (CORRECT_PERCENT >= ops.passWhen) {
                    status = 'PASSED';
                    console.log(chalk.greenBright('Done testing'));
                    console.log('Status: ' + chalk.greenBright(status) + ' ' + CORRECT_PERCENT + '%');
                } else {
                    status = 'FAILED';
                    console.log(chalk.greenBright('Done testing'));
                    console.log('Status: ' + chalk.redBright(status) + ' ' + CORRECT_PERCENT + '%');
                }
            }
        }
        if (status === undefined) {
            status = 'FAILED';
        }
        return {
            stat: status,
            val: CORRECT_PERCENT
        };
    }

    /**
     * Ein genetischer Algorythmus soll eine Population über Generationen mutieren und trainieren.
     * @param rate Eine Zahl an der die Population mutieren soll.
     */
    public mutate(rate: number): void {
        console.log(chalk.yellowBright('Feature not implemented yet'));
        return;
    }

    /**
     * Loggt das gegebene Netzwerk in die Konsole.
     * @param net Neurales Netzwerk welches ausgegeben werden soll.
     */
    public static log(net: NeuralNet): void {
        let numHiddenNodes = 0;
        for (let i = 0; i < net.numHiddenLayers; i++) {
            numHiddenNodes += net.hiddenLayerNodes[i];
        }

        let resultText = `\nNeural Network: \n`;
        resultText += ` NODES:\n`;
        resultText += `  Input nodes: ${net.inputNodes}\n`;
        resultText += `  Hidden nodes: ${numHiddenNodes}\n`;
        resultText += `  Output nodes: ${net.outputNodes}\n`;
        resultText += `\n`;
        resultText += ` HIDDEN LAYERS:\n`;

        for (let i = 0; i < net.numHiddenLayers; i++) {
            resultText += `  Hidden Layer ${i + 1}:\n`;
            resultText += `   Nodes: ${net.hiddenLayerNodes[i]}\n`;
        }

        resultText += ` LEARNING RATE: ${net.learningRate}`;

        console.log(resultText);
    }

    /**
     * Loggt das Netzwerk in die Konsole.
     */
    public log(): void {
        NeuralNet.log(this);
    }

    /**
     * Gibt das Netzwerk als String zurück um es z.B. zu speichern.
     * @returns Einen String welcher z.B. in JSON Format gespeichert werden kann.
     */
    public emit(): string {
        const res = JSON.stringify(this, undefined, 1);
        return res;
    }
}
