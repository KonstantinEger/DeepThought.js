/**
 * ### Matrix Library von [Daniel Shiffmann](https://github.com/shiffmann)
 * - mit eigenen Erweiterungen
 * @constructor Erstellt eine Matrix mit gegebener Anzahl an Rows & Columns.
 * @method add: (_static_) Addiert Zwei Matrizen.
 * @method subtract: (_static_) Subtrahiert Zwei Matrizen von einander,
 * @method multiply: (_static_) Multipliziert Zwei Matrizen.
 * @method fromArray: (_static_) Gibt eine Matrix von einem gegebenen Array zurück.
 * @method toArray: (_static_) Gibt ein Array mit allen Inhalten der Matrix zurück.
 * @method toArray2D: (_static_) Gibt ein zweidimensionales Array mit allen Inhalten der Matrix zurück.
 * @method transpose: (_static_) Transponiert eine gegebene Matrix.
 * @method copy: (_static_) Kopiert eine gegebene Matrix.
 * @method map: (_static_) Wendet eine gegebene Funktion auf jedes Element der Matrix an.
 * @method add: Addiert eine Zahl oder Matrix.
 * @method subtract: Subtrahiert eine Zahl oder Matrix.
 * @method multiply: Multipliziert eine Zahl oder Matrix.
 * @method map: Wendet auf jedes Element der Matrix eine gegebene Funktion an.
 * @method copy: Kopiert die Matrix.
 * @method randomize: Weißt jedem Element in der Matrix eine pseudo-zufällige Zahl zw. -1.0 & 1.0 zu.
 * @method print: Loggt die Matrix in anschaulicher Form zur Konsole.
 * @method fill: Weißt jedem Element der Matrix eine gegebene Zahl zu (sonst 0).
 */
export default class Matrix {
    public rows: number;
    public cols: number;
    public data: number[][];

    constructor(r: number, c: number, n?: number) {
        this.rows = r;
        this.cols = c;
        this.data = [];
        if (!n) {
            n = 0;
        }

        for (let i = 0; i < this.rows; i++) {
            this.data[i] = [];
            for (let j = 0; j < this.cols; j++) {
                this.data[i][j] = n;
            }
        }
    }

// === STATIC MATRIX-MATH ===

    public static add(a: Matrix, b: Matrix): Matrix {
        const result = new Matrix(a.rows, a.cols);
        for (let i = 0; i < a.rows; i++) {
            for (let j = 0; j < a.cols; j++) {
                result.data[i][j] = a.data[i][j] + b.data[i][j];
            }
        }
        return result;
    }

    public static subtract(a: Matrix, b: Matrix): Matrix {
        const result = new Matrix(a.rows, a.cols);
        for (let i = 0; i < a.rows; i++) {
            for (let j = 0; j < a.cols; j++) {
                result.data[i][j] = a.data[i][j] - b.data[i][j];
            }
        }
        return result;
    }

    public static multiply(a: Matrix, b: Matrix): Matrix {
        // Matrix Product
        if (a.cols !== b.rows) {
            console.error('Cols of A must match rows of B.');
            return new Matrix(0, 0);
        } else {
            const result = new Matrix(a.rows, b.cols);
            for (let i = 0; i < result.rows; i++) {
                for (let j = 0; j < result.cols; j++) {
                    // Dot Product of values in col
                    let sum = 0;
                    for (let k = 0; k < a.cols; k++) {
                        sum += a.data[i][k] * b.data[k][j];
                    }
                    result.data[i][j] = sum;
                }
            }
            return result;
        }

    }

// === STATIC MATRIX-UTILS ===

    public static fromArray(arr: number[]): Matrix {
        const m = new Matrix(arr.length, 1);
        for (let i = 0; i < arr.length; i++) {
            m.data[i][0] = arr[i];
        }
        return m;
    }

    public static toArray(m: Matrix): number[] {
        const result = [];
        for (let i = 0; i < m.rows; i++) {
            for (let j = 0; j < m.cols; j++) {
                result.push(m.data[i][j]);
            }
        }
        return result;
    }

    public static toArray2D(m: Matrix): number[][] {
        return m.data;
    }

    public static transpose(matrix: Matrix): Matrix {
        const result = new Matrix(matrix.cols, matrix.rows);
        for (let i = 0; i < matrix.rows; i++) {
            for (let j = 0; j < matrix.cols; j++) {
                result.data[j][i] = matrix.data[i][j];
            }
        }
        return result;
    }

    public static copy(m: Matrix): Matrix {
        const result = new Matrix(m.rows, m.cols);
        for (let i = 0; i < m.rows; i++) {
            for (let j = 0; j < m.cols; j++) {
                result.data[i][j] = m.data[i][j];
            }
        }
        return result;
    }

    public static map(matrix: Matrix, func: (n: number) => number): Matrix {
        const result = new Matrix(matrix.rows, matrix.cols);

        for (let i = 0; i < matrix.rows; i++) {
            for (let j = 0; j < matrix.cols; j++) {
                const val = matrix.data[i][j];
                result.data[i][j] = func(val);
            }
        }
        return result;
    }

// === INSTANCE MATRIX-MATH ===

    public add(n: number | Matrix): void {
        if (n instanceof Matrix) {
            for (let i = 0; i < this.rows; i++) {
                for (let j = 0; j < this.cols; j++) {
                    this.data[i][j] += n.data[i][j];
                }
            }
        } else {
            for (let i = 0; i < this.rows; i++) {
                for (let j = 0; j < this.cols; j++) {
                    this.data[i][j] += n;
                }
            }
        }
    }

    public subtract(n: number | Matrix): void {
        if (n instanceof Matrix) {
            for (let i = 0; i < this.rows; i++) {
                for (let j = 0; j < this.cols; j++) {
                    this.data[i][j] -= n.data[i][j];
                }
            }
        } else {
            for (let i = 0; i < this.rows; i++) {
                for (let j = 0; j < this.cols; j++) {
                    this.data[i][j] -= n;
                }
            }
        }
    }

    public multiply(n: number | Matrix): void {
        if (n instanceof Matrix) {
            for (let i = 0; i < this.rows; i++) {
                for (let j = 0; j < this.cols; j++) {
                    this.data[i][j] *= n.data[i][j];
                }
            }
        } else {
            // Scalar product
            for (let i = 0; i < this.rows; i++) {
                for (let j = 0; j < this.cols; j++) {
                    this.data[i][j] *= n;
                }
            }
        }
    }

// === INSTANCE MATRIX-UTILS ===

    public map(func: (n: number) => number): void {
        // Apply a function to every object in the Matrix
        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.cols; j++) {
                const val = this.data[i][j];
                this.data[i][j] = func(val);
            }
        }
    }

    public copy(): Matrix {
        const m = new Matrix(this.rows, this.cols);
        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.cols; j++) {
                m.data[i][j] = this.data[i][j];
            }
        }
        return m;
    }

    public randomize(): void {
        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.cols; j++) {
                this.data[i][j] = Math.random() * 2 - 1;
            }
        }
    }

    public print(): void {
        console.table(this.data);
    }

    public fill(n?: number): void {
        if (!n) {
            n = 0;
        }

        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.cols; j++) {
                this.data[i][j] = n;
            }
        }
    }
}
