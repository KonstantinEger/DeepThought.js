import Matrix from './Matrix/matrix';
import NeuralNet from './NeuralNet/neuralnet';
import { load } from './NeuralNet/neuralnet.lib';

export {
    NeuralNet as NeuralNetwork,
    Matrix,
    load as loadNetwork
};
