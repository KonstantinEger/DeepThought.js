const path = require('path');

module.exports = {
    entry: './dist/index.js',
    mode: 'production',
    output: {
        filename: 'DeepThought.js',
        path: path.resolve(__dirname, 'build'),
        library: 'dt',
        libraryTarget: 'var'
    }
}