# DeepThought.js

#### Machine-Learning im Browser.

## Was ist DeepThought.js

DeepThought.js ist eine JavaScript Library für Machine Learning im Browser. Es baut auf der [Toy-Neural-Network-JS](https://github.com/CodingTrain/Toy-Neural-Network-JS/) Library von [Daniel Shiffmann](https://github.com/shiffman) auf und erweitert diese mit der möglichkeit für Multi-Layer-Netzwerke. Mehrere Trainingsfunktionen wie Back-Propagation und ein Genetischer Algorythmus sollen mit eingebaut werden.

## Getting started
- #### CDN
```html
<script src="https://lavakonsti.github.io/DeepThought.js/beta/DeepThought.min.js" charset="utf-8"></script>
```
- #### NPM
```bash
$ npm i deep-thought.js --save
```
- #### >> [DOWNLOAD](https://github.com/LavaKonsti/DeepThought.js/releases)

## Dokumentation
Eine vollständige Dokumentation der Library ist [hier](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs) verfügbar.

## Example

So würde es aussehen die Library über NPM zu installieren und zu nutzen.
> Project
```bash
root
|- package.json
|- src/
   |- app.js
```

> Install
```bash
$ npm i deep-thought.js --save
```

> Project
```bash
root
|- node_modules/
|- package.json
|- package-lock.json
|- src/
   |- app.js
```

> ./src/app.js

```javascript
const dt = require('deep-thought.js');

const nn = new dt.NeuralNetwork();

// init and add the layers
let layers = [
  { type: 'input',  nodes: 2},
  { type: 'hidden', nodes: 4},   // NOTE: order of the hidden layers is important
  { type: 'hidden', nodes: 4},
  { type: 'output', nodes: 2}
];

// add the initialized layers to the Network
nn.addLayers(layers);

// train the neural network
nn.train([trainingDataSet], {
  iterations: 3000000,
  print: true,
  test: true
});
// Use it
nn.predict([data], (result) => {
  console.log(result);
});
```

## Getting Involved
### 1. Repository forken
### 2. Befehl im Root-Folder:
```bash
$ npm install
```
### 3. Projekt Bearbeiten
### 4. Befehl für compile & build:
```bash
$ npm run build
```
### 5. Eigene Produktivität bestaunen :
Das endgültige Build-Produkt ist `./dist/DeepThought.js`.
Der Ordner `./temp/` ist nur der compilierte JavaScript code.
Dieser wird für das NPM Package verwendet.

### Commit Legende
- :boat: New Release
- :art: Format/Structure
- :memo: Documentation
- :boom: new Feature
- :bug: Bugfix
- :fire: Code or Files deleted
- :wrench: Work In Progress
- :star2: Enhancement

## License

Die Library ist unter der MIT License verfügbar.

Mehr über die License [hier](https://github.com/LavaKonsti/DeepThought.js/blob/master/LICENSE).
