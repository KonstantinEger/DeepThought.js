# Matrix

Matrix Library für Berechnungen und Umformungen von Matrizen.
## Komponenten
- cols: number
- rows: number
- data: Array>Array

## API: static
### Matrix Math
---
### Addition | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L75-L83)
```
Die static Methode: dt.Matrix.add(a, b) nimmt als Argumente zwei Matrizen: 
A und B. Jedes Element der Matrix A wird mit dem Element an der gleichen Position in Matrix 
B addiert.
Die gegebenen Matrizen werden nicht verändert.
```
> _Get_: Matrix, Matrix <br>
> _Return_: Matrix
```javascript
let a = new dt.Matrix(3,3);
let b = new dt.Matrix(3,3);
let c = dt.Matrix.add(a, b);
// |3,2,0|        |1,2,1|       |4,4,1|
// |5,4,1|   +    |4,3,8|   =   |9,7,9|
// |6,2,0|        |2,1,2|       |8,3,2|
```
---
### Subtraktion | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L65-L73)
```
Die static Methode: dt.Matrix.subtract(a, b) nimmt als Argumente zwei 
Matrizen: A und B. Jedes Element der Matrix B wird von dem Element an der gleichen Position 
in Matrix A subtrahiert.
Die gegebenen Matrizen werden nicht verändert.
```
> _Get_: Matrix, Matrix <br>
> _Return_: Matrix
```javascript
let a = new dt.Matrix(3,3);
let b = new dt.Matrix(3,3);
let c = dt.Matrix.subtract(a, b);
// |3,2,0|        |1,2,1|       |2,0,-1|
// |5,4,1|   -    |4,3,8|   =   |1,1,-7|
// |6,2,0|        |2,1,2|       |4,1,-2|
```
---
### Multiplikation | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L43-L63)
> ACHTUNG: Anzahl der Spalten in Matrix A muss gleich sein wie Anzahl der Zeilen in Matrix B. Für zwei gleiche Matrizen: [transpose](#transpose)
```
Die static Methode: dt.Matrix.multiply(a, b) nimmt als Argumente 2 Matrizen. Bei der 
Multiplikation wird jedes Element aus der n'ten Zeile in A mit jedem Element aus der n'ten Zeile in B
multipliziert.
Die gegebenen Matrizen werden nicht verändert.
```
> _Get_: Matrix, Matrix <br>
> _Return_: Matrix
```javascript
let a = new dt.Matrix(2,3);
let b = new dt.Matrix(3,2);
let c = dt.Matrix.multiply(a, b);
//                | 7, 8|       
// |1,2,3|   *    | 9,10|   =   | 58, 64|
// |4,5,6|        |11,12|       |139,154|
```
---
### Matrix Utils
### From Array | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L25-L31)
```
Die static Methode: dt.Matrix.fromArray(array) nimmt als Argument ein
Array mit Zahlen und wandelt diese in eine Matrix (Vector) um. Jedes Element
in dem Array wird in eine neue Zeile in der Matrix eingetragen.
```
> _Get_: Array: number[] <br>
> _Return_: Matrix
```javascript
let arr = [1,2,3];
let m = dt.Matrix.fromArray(arr);
//    | 1 |
//    | 2 |
//    | 3 |
```
---
### To Array | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L33-L41)
```
Die static Methode: dt.Matrix.toArray(matrix) nimmt als Argument eine Matrix und wandelt diese in ein Array um.
```
> _Get_: Matrix <br>
> _Return_: Array: number[]
```javascript
//     |1,2|
// m = |3,4|

let mArr = dt.Matrix.toArray(m);
//  => [1, 2, 3, 4];
```
---
### Transpose | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L85-L93)
```
Die static Methode: dt.Matrix.transpose(matrix) nimmt als Argument eine Matrix und wandelt diese in eine
transponierte Matrix um. D.h. Zeile n ist nach dem transponieren Spalte n.
Die gegebene Matrix wird nicht verändert.
```
> _Get_: Matrix <br>
> _Return_: Matrix
```javascript
//    |1,2,3|        |1,4,7|
//    |4,5,6|   =>   |2,5,8|
//    |7,8,9|        |3,6,9|
```
---
### Map | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L105-L115)
```
Die static Methode: dt.Matrix.map(matrix, func) nimmt als Argument eine Matrix und eine Funktion.
Diese Funktion wird auf jedes Element in der Matrix angewand.
Die gegebene Matrix wird nicht verändert.
```
> _Get_: Matrix, function <br>
> _Return_: Matrix
```javascript
function addTwo(val) {
    return val + 2;
}
//     |1,2|
// m = |3,4|

let m_added = dt.Matrix.map(m, addTwo);
//  => |3,4|
//     |5,6|
```
---
### Copy | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L95-L103)
```
Die static Methode: dt.Matrix.copy(matrix) nimmt als Argument eine Matrix, kopiert sie und gibt sie zurück.
Die gegebene Matrix wird nicht verändert.
```
> _Get_: Matrix <br>
> _Return_: Matrix
```javascript
let m = new Matrix(2,2);
let m2 = dt.Matrix.copy(m);

if (m1 == m2) {
    console.log("true");
}
```
---
## API: instance
### Math
### Addition | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L146-L152)
```
Die Methode: .add(number) nimmt als Argument eine Zahl welche dann auf
jedes Element in der Matrix addiert wird.
```
> _Get_: Number <br>
> _Return_: Void
```javascript
//     |1,2|
// m = |3,4|
let n = 3;
m.add(n);
//     |4,5|
// m = |6,7|
```
---
### Subtraction | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L154-L160)
```
Die Methode: .subtract() nimmt als Argument eine Zahl welche dann von
jedem Element in der Matrix subtrahiert wird.
```
> _Get_: Number <br>
> _Return_: Void
```javascript
//     |6,7|
// m = |8,9|
let n = 3;
m.subtract(n);
//     |3,4|
// m = |5,6|
```
---
### Multiplikation | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L117-L124)
```
Die Methode: .multiply() nimmt als Argument eine Zahl welche dann mit
jedem Element in der Matrix multipliziert wird.
```
> _Get_: Number <br>
> _Return_: Void
```javascript
//     |1,2|
// m = |3,4|
let n = 3;
m.multiply(n);
//     |1, 6|
// m = |9,12|
```
---
### Utils
### Map | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L126-L134)
```
Die Methode: .map() nimmt als Argument eine Funktion welche auf
jedes Element in der Matrix angewendet wird.
```
> _Get_: Funktion <br>
> _Return_: Void
```javascript
function addTwo(val) {
    return val + 2;
}

//     |1,2|
// m = |3,4|
m.map(addTwo);
//     |3,4|
// m = |5,6|
```
---
### Randomize | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L172-L178)
```
Die Methode: .randomize() füllt jedes Element der Matrix mit einer
pseudo-zufälligen Zahl zwischen -1 und 1.
```
> _Get_: --- <br>
> _Return_: Void
```javascript
// Zum Beispiel:
let m = new Matrix(2,2);
m.randomize();
//     |-0.5628726422613233,0.6766988790842214|
// m = |0.37807798992105646,0.2551438233749126|
```
---
### Print | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L180-L182)
```
Die Methode: .print() logt die Matrix als Tabelle in die Konsole.
```
> _Get_: --- <br>
> _Return_: Void
---
### Fill | [Source](https://github.com/LavaKonsti/DeepThought.js/blob/a14307c82b587daef26d7c9545b44dea0a5ddddc/src/Matrix.ts#L184-L195)
```
Die Methode: .fill() füllt jedes Element der Matrix mit einer gegebenen Zahl
oder mit 0.
```
> _Get_: Number? <br>
> _Return_: Void
---