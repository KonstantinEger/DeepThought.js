# DeepThought.js
#### Machine-Learning im Browser.

DeepThought.js ist eine JavaScript Library für Machine Learning im Browser. Es baut auf der [Toy-Neural-Network-JS](https://github.com/CodingTrain/Toy-Neural-Network-JS/) Library von [Daniel Shiffmann](https://github.com/shiffman) auf und erweitert diese mit der möglichkeit für Multi-Layer-Netzwerke. Mehrere Trainingsfunktionen wie Back-Propagation und ein Genetischer Algorythmus sollen mit eingebaut werden.

## Getting Started

```javascript
// initialize the neural network
let nn = new dt.NeuralNetwork();

// init and add the layers
let layers = [
  { type: 'hidden', nodes: 4},   // NOTE: order of the hidden layers is important
  { type: 'hidden', nodes: 4},
  { type: 'output', nodes: 2}
];

nn.addLayers(layers);

// train the neural network
nn.train([trainingData], [targetData], (errorRate) => {
  console.log(errorRate);

  use();
}, iterations?); // NOTE: default is 10000

function use() {
  let result = nn.predict([someData]);
}
```
---
## [Neural Network](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Neural%20Network#neural-network) Dokumentation

- ### [Komponenten](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Neural%20Network#komponenten)
- ### [API](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Neural%20Network#api)
  - [new dt.NeuralNetwork()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Neural%20Network#new-neural-network)
  - [.addLayers()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Neural%20Network#add-layers)
  - [.train()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Neural%20Network#training)
  - [.test()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Neural%20Network#testing)
  - [.predict()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Neural%20Network#predict)
  - [.log()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Neural%20Network#log)
  - [.emit()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Neural%20Network#emit)
  - [dt.loadNetwork()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Neural%20Network#load)
---
## [Matrix](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#matrix) Dokumentation
### Static:
- #### Math ([Gesamt](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#matrix-math))
  - [dt.Matrix.add()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#addition--source)
  - [dt.Matrix.subtract()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#subtraktion--source)
  - [dt.Matrix.multiply()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#multiplikation--source)
- #### Utils ([Gesamt](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#matrix-utils))
  - [dt.Matrix.fromArray()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#from-array--source)
  - [dt.Matrix.toArray()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#to-array--source)
  - [dt.Matrix.transpose()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#transpose--source)
  - [dt.Matrix.map()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#map--source)
  - [dt.Matrix.copy()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#copy--source)

### Instance:
- #### Math ([Gesamt](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#math))
  - [.add()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#addition--source-1)
  - [.subtract()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#subtraction--source)
  - [.multiply()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#multiplikation--source-1)
- #### Utils ([Gesamt](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#utils))
  - [.map()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#map--source-1)
  - [.randomize()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#randomize--source)
  - [.print()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#print--source)
  - [.fill()](https://github.com/LavaKonsti/DeepThought.js/tree/master/docs/Matrix#fill--source)
