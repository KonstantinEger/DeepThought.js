# Neural Network
Eine Mashine-Learning Library für den Browser.
## Komponenten
- inputNodes: number
- hiddenLayerNodes: number[]
- numHiddenLayers: number
- outputNodes: number
- learningRate: number
- totalError: number
---
- hiddenLayers: HiddenLayer[]
- outputLayer: OutputLayer

## API
### New Neural Network
```
Beim erstellen des neuralen Netzwerkes wird nur die Grundstruktur für das
Netzwerk ausgelegt. D.h. es sind noch keine Layer und keine Nodes vorhanden
```
> JavaScript:
```javascript
let nn = new dt.NeuralNetwork();
```
---
### Add Layers
```
Mit dieser Methode werden Layer mit ihren Nodes zum Netzwerk hinzugefügt.
Die Funktion nimmt als Argumente ein Array von Objekten die bestimmte
Dinge beinhalten müssen.

1. type: INPUT | HIDDEN | OUTPUT
2. nodes: number
```
> _Get_: Array <br>
> _Return_: Void
```javascript
let nn = new dt.NeuralNetwork();
let nnLayers = [
    {type: INPUT , nodes: 2},
    {type: HIDDEN, nodes: 4},
    {type: HIDDEN, nodes: 3},
    {type: OUTPUT, nodes: 2},
];

nn.addLayers(nnLayers);
// -InputLayer:     2 Nodes
// -HiddenLayer_1:  4 Nodes
// -HiddenLayer_2:  3 Nodes
// -OutputLayer:    2 Nodes
```
> NOTE: Die Reihenfolge der HiddenLayer wird so ins Netzwerk übertragen wie sie im Array festgelegt wurde.
---
### Training
```
Die Methode: .train() ist ein Back-Propagation Algorythmus welcher als
Argumente ein Array mit den Trainings-Daten (Objekt mit "input" und "target" Array)
und optional ein Objekt mit Optionen nimmt. Diese Optionen wären "iterations", "print", "test". Diese
Eigenschaften sind optional. Der Algorythmus nähert sich dem Target immer weiter an.
```
> _Get_: TrainingDataArray, {iterations?: Number, print?: Boolean, test?: Boolean} <br>
> _Return_: Void
```javascript
let nn = new dt.NeuralNetwork();

nn.addLayers([
    {type: INPUT , nodes: 2},
    {type: HIDDEN, nodes: 4},
    {type: HIDDEN, nodes: 3},
    {type: OUTPUT, nodes: 1},
]);

let trainingOps = {
    iterations: 5000000,
    print: true,
    test: true
};

nn.train([
    {
        input: [1,0],
        target: [1]
    },
    // ...
], trainingOps);
```
---
### Testing
```
Die Methode: .test() nimmt als Argumente ein Test-Datenset (Array mit "input" und "target" Objekten)
und optional ein Objekt mit Optionen. Diese Optionen wären "iterations" und "print".
Beide Optionen sind optional. Die Methode gibt eine Zahl zw. 0-1 zurück welche das Testergebnis
repräsentiert.
```
> _Get_: TestingDataArray, {iterations?: Number, print?: Boolean} <br>
> _Return_: Number
```javascript
let nn = new dt.NeuralNetwork();

// ... add Layers, train and stuff ...

nn.test(testingDataSet, {iterations: 2000, print: true});
```
---
### Predict
```
Die Methode: .predict() ist ein Feed-Forward Algorythmus. Er nimmt als
Argumente ein Input-Array (Länge gleich Anzahl der Input-Nodes) und berechnet
daraus ein Ergebnis basierend auf den Trainingsdaten die das Netzwerk vorher
bekommen hat und optional eine Callback(Ergebnis). Das Ergebnis wird as Array zurück gegeben.
```
> _Get_: InputArray, Callback <br>
> _Return_: Array
```javascript
let nn = new dt.NeuralNetwork();

nn.addLayers([
    {type: INPUT , nodes: 2},
    {type: HIDDEN, nodes: 2},
    {type: OUTPUT, nodes: 1},
]);
nn.train(/* ... */);

nn.predict([1,0], (res) => {
    console.log(result)
// result = [9.76578656565];
});
```
---
### Log
```
Die Methode: .log() (static & instance) logt alle wichtigen Informationen über
das Netzwerk in die Konsole.
```
> _Get_: --- (wenn static: Netzwerk)<br>
> _Return_: Void
```javascript
let nn = new dt.NeuralNetwork();
nn.log();
// or...
dt.NeuralNetwork.log(nn);
```
---
### Emit
```
Die Methode: .emit() nimmt keine Argumente und gibt das Neurale Netzwerk 
als String zurück um es z.B. in einer .json Datei zu speichern.
```
> _Get_: Network <br>
> _Return_: String
```javascript
let nn = new dt.NeuralNetwork();

// ... add layers and train and stuff ...

let data = nn.emit();
// => {
//     "inputNodes": 1,
//     "hiddenLayerNodes": [
//      4
//     ],
//     "hiddenLayers": [
//      {
//       "w_f_t": {
//        "rows": 4,
//        "cols": 1,
//        "data": [
//         [
//          0.8546625400123302
//         ],
//         [
//  ...
```
---
### Load
```
Die static Funktion: dt.loadNetwork() nimmt als Argument einen String welcher durch die
[.emit()](#emit) Methode erstellt wurde. Sie gibt eine genaue Kopie des gegebenen Netzwerkes
zurück.
```
> _Get_: String <br>
> _Return_: NeuralNetwork
```javascript
let nn = new dt.NeuralNetwork();
// ... add layers and train and stuff ...
let savedNetwork = nn.emit();

let newNet = dt.loadNetwork(savedNetwork);
// nn == newNet
```